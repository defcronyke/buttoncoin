@echo off

if %1==-s (
	echo %3 %4 %5 | ncat.exe -i 1 %2 8081 2> nul
	goto :end
)

echo %* | ncat.exe -i 1 localhost 8081 2> nul
:end
