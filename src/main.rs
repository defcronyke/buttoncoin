use buttoncoin::ButtonCoin;
use pretty_env_logger;
use std::env;
// use std::thread;
// use log::info;
use std::error::Error;
// use tokio;
// use tokio::io::{AsyncReadExt, AsyncWriteExt};
// use tokio::net::TcpListener;

// #[tokio::main]
fn main() -> Result<(), Box<dyn Error>> {
    init_logger();

    ButtonCoin::start()?;

    Ok(())
}

fn init_logger() {
    env::var("RUST_LOG").unwrap_or_else(|_| {
        let loglevel = "info".to_string();
        env::set_var("RUST_LOG", loglevel.clone());
        loglevel
    });
    pretty_env_logger::init();
}
