use base64::{engine::general_purpose, Engine as _};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512};
use std::fmt;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Transaction {
    pub timestamp: u128,
    pub amount: u128,
    pub from_address: String,
    pub to_address: String,
    pub sha512_hash: String,
}

impl fmt::Display for Transaction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}

impl Transaction {
    pub fn new(
        genesis_seed_hash: String,
        amount: u128,
        from_address: &str,
        to_address: &str,
    ) -> Transaction {
        let mut t = Transaction {
            timestamp: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos(),
            amount,
            from_address: from_address.to_string(),
            to_address: to_address.to_string(),
            sha512_hash: "".to_string(),
        };

        let mut h = Sha512::new();
        h.update(format!("{}{}", genesis_seed_hash, t).as_bytes());
        t.sha512_hash = general_purpose::URL_SAFE_NO_PAD.encode(h.finalize());

        t
    }

    pub fn valid_to_address(&self) -> bool {
        self.to_address.len() == 43 || self.to_address == "0"
    }
}
