#![crate_name = "buttoncoin"]

pub mod buttoncoin;
pub mod chainlink;
pub mod command;
pub mod config;
pub mod db;
pub mod peer;
pub mod transaction;

pub use crate::buttoncoin::ButtonCoin;

#[cfg(test)]
mod tests {
    use crate::buttoncoin::ButtonCoin;
    // use std::str;

    #[test]
    fn it_works() {}

    #[test]
    fn buttoncoin_new() {
        ButtonCoin::new();
    }

    #[test]
    fn buttoncoin_genesis_seed_is_set() {
        let buttoncoin = ButtonCoin::new();

        println!("Genesis Seed: {}", buttoncoin.genesis_seed);

        assert!(buttoncoin
            .genesis_seed
            .eq(&include_str!(".genesis.txt").to_string()));
    }

    #[test]
    fn buttoncoin_genesis_seed_hash_is_correct() {
        let buttoncoin = ButtonCoin::new();

        println!("Genesis Seed Hash: {}", buttoncoin.genesis_seed_hash);

        assert!(buttoncoin
            .genesis_seed_hash
            .eq(buttoncoin.hash_genesis_seed().as_str()));
    }

    #[test]
    fn buttoncoin_new_save_and_load_rsa_keypair() {
        let priv_filename = "test-bc.key";
        let pub_filename = "test-bc.key.pub";

        let mut bc = ButtonCoin::new();

        bc.new_rsa_keypair();

        println!("RSA Public Key:\n{:?}", bc.rsa_public_key.as_ref().unwrap());

        bc.save_rsa_keypair(priv_filename, pub_filename).unwrap();

        bc.load_rsa_keypair(priv_filename, pub_filename).unwrap();
    }
}
