use rocksdb::{ColumnFamilyDescriptor, Options, DB};

pub struct Db {
    pub db: Option<DB>,
    pub db_opts: Option<Options>,
}

impl Db {
    pub fn new() -> Db {
        Db {
            db: None,
            db_opts: None,
        }
    }

    pub fn open(&mut self, db_path: &str) {
        let mut db_path = db_path;

        if db_path == "" {
            db_path = "./.bc-chain.rocksdb";
        }

        let mut hash_cf_opts = Options::default();
        hash_cf_opts.set_max_write_buffer_number(16);

        let mut timestamp_cf_opts = Options::default();
        timestamp_cf_opts.set_max_write_buffer_number(16);

        let mut transaction_cf_opts = Options::default();
        transaction_cf_opts.set_max_write_buffer_number(16);

        let hash_cfd = ColumnFamilyDescriptor::new("hash", hash_cf_opts);
        let timestamp_cfd = ColumnFamilyDescriptor::new("timestamp", timestamp_cf_opts);
        let transaction_cfd = ColumnFamilyDescriptor::new("transaction", transaction_cf_opts);

        let mut db_opts = Options::default();
        db_opts.create_missing_column_families(true);
        db_opts.create_if_missing(true);
        self.db_opts = Some(db_opts.clone());

        let db = DB::open_cf_descriptors(
            &db_opts,
            db_path,
            vec![hash_cfd, timestamp_cfd, transaction_cfd],
        )
        .unwrap();

        self.db = Some(db);
    }

    pub fn close(&mut self, db_path: &str) {
        let mut db_path = db_path;

        if db_path == "" {
            db_path = "./.bc-chain.rocksdb";
        }

        let _ = DB::destroy(self.db_opts.as_ref().unwrap(), db_path);

        self.db = None;
        self.db_opts = None;
    }
}
