use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::io::Read;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {
    pub listen: String,
}

impl Config {
    pub fn new(mut filename: &str) -> Config {
        if filename == "" {
            filename = "config.json"
        }

        let f = File::open(filename).unwrap();
        let mut reader = BufReader::new(f);
        let mut buffer = Vec::new();

        reader.read_to_end(&mut buffer).unwrap();

        serde_json::from_slice(&buffer[..]).unwrap()
    }
}
