use crate::chainlink::ChainLink;
use crate::command::Command;
use crate::config::Config;
use crate::db::Db;
use crate::peer::Peer;
use crate::transaction::Transaction;
use base64::{engine::general_purpose, Engine as _};
use log::{error, info, warn};
use pkcs1::{self, EncodeRsaPublicKey};
use rand;
use rocksdb::{Direction, IteratorMode};
use rsa::{RsaPrivateKey, RsaPublicKey};
use sha2::{Digest, Sha256, Sha512};
use std::cmp;
use std::error::Error;
use std::net::IpAddr;
use std::str::FromStr;
use std::sync::Arc;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::{env, str};
use tokio;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::Mutex;

pub type ButtonCoinShared = Arc<Mutex<ButtonCoin>>;

/// The application struct.
pub struct ButtonCoin {
    pub config: Config,

    /// A seed added at the time of genesis.
    pub genesis_seed: String,

    /// A hash of the genesis seed.
    pub genesis_seed_hash: String,

    /// A public key for the client.
    pub rsa_public_key: Option<RsaPublicKey>,

    /// A private key for the client.
    rsa_private_key: Option<RsaPrivateKey>,

    /// A public key for the burn address.
    pub rsa_public_burn_key: Option<RsaPublicKey>,

    /// A private key for the burn address.
    rsa_private_burn_key: Option<RsaPrivateKey>,

    /// The current chain link.
    pub chain: Option<ChainLink>,

    /// A struct for accessing the database.
    pub db: Db,

    pub peers: Vec<Peer>,
}

impl ButtonCoin {
    /// Returns a new application instance.
    pub fn new() -> ButtonCoin {
        let mut bc = ButtonCoin {
            config: Config::new(""),
            genesis_seed: include_str!(".genesis.txt").to_string(),
            genesis_seed_hash: "kJGqW9XOeBpoxUL47IXXVZR4RpQ0FelEREn4f0DXwAKjTiMfsIvW0jcO1B2HybYDo9gytl1R_eG7i5t7gBpxAg==".to_string(),
            rsa_public_key: None,
            rsa_private_key: None,
            rsa_public_burn_key: None,
            rsa_private_burn_key: None,
            chain: None,
            db: Db::new(),
            peers: vec![],
        };

        bc.load_rsa_keypair("", "").unwrap_or_else(|_| {
            bc.new_rsa_keypair();
            bc.save_rsa_keypair("", "").unwrap();
        });

        bc.load_rsa_burn_keypair("", "").unwrap();

        bc.db.open("");

        bc
    }

    /// Start the application.
    #[tokio::main]
    pub async fn start() -> Result<(), Box<dyn Error>> {
        let bc = Arc::new(Mutex::new(ButtonCoin::new()));

        let config = bc.clone().lock().await.config.clone();

        let addr = env::args().nth(1).unwrap_or_else(|| config.listen);

        let listener = TcpListener::bind(&addr).await?;
        info!("listening on: {}", addr);

        loop {
            let (socket, _) = listener.accept().await.unwrap();
            info!("connection accepted: {:?}", socket);

            let bc = bc.clone();

            tokio::spawn(async move {
                ButtonCoin::process(socket, bc).await;
            });
        }
    }

    /// Process commands being sent to the application over TCP.
    pub async fn process(mut socket: TcpStream, bc: ButtonCoinShared) {
        let mut buf = vec![0; 1024];

        loop {
            let n = socket.read(&mut buf).await.unwrap_or_default();

            if n == 0 {
                return;
            }

            let mut remote = false;

            let input_str = str::from_utf8(&buf[0..n]).unwrap_or_default();

            let output_str = {
                let mut bc = bc.lock().await;
                // let config = bc.config.clone();
                // let listen_parts: Vec<&str> = config.listen.split(":").collect();
                let port = 8081u16;
                // if listen_parts.len() == 2 {
                //     port = listen_parts[1].parse().unwrap();
                // }

                if !socket.peer_addr().unwrap().ip().is_loopback() {
                    remote = true;
                }

                bc.execute_command(input_str, remote, socket.peer_addr().unwrap().ip(), port)
                    .await
            };

            socket
                .write_all(&format!("{}\n", output_str).as_bytes())
                .await
                .expect("failed to write data to socket");
        }
    }

    /// Run a specific command.
    pub async fn execute_command(
        &mut self,
        mut input_str: &str,
        remote: bool,
        address: IpAddr,
        mut port: u16,
    ) -> String {
        // Remove whitespace around string if present.
        input_str = input_str.trim();

        // Remove newline character if present.
        if input_str.ends_with("\n") {
            input_str = &input_str[0..(input_str.len() - 1)];
        }

        // Remove Windows newline character if present.
        if input_str.ends_with("\r") {
            input_str = &input_str[0..(input_str.len() - 1)];
        }

        let input_parts: Vec<&str> = input_str.split(" ").collect();

        if input_parts.len() < 1 {
            return "".to_string();
        }

        let input_cmd = input_parts[0];
        let mut input_args: Vec<&str> = vec![];

        if input_parts.len() > 1 {
            input_args = input_parts[1..input_parts.len()].to_vec();
        }

        let input_enum = Command::from_str(input_cmd).unwrap_or(Command::Invalid);

        match input_enum {
            Command::Invalid => "error: invalid command".to_string(),
            Command::Hello => {
                if remote {
                    if input_args.len() > 1 {
                        return "error: invalid arguments: hello [port | 8081]".to_string();
                    } else if input_args.len() == 1 {
                        port = input_args[0].parse().unwrap();
                    }

                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    self.hello()
                } else {
                    "hi myself!".to_string()
                }
            }
            Command::RsaPubKey => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    self.get_rsa_public_key_pem().unwrap()
                }
            }
            Command::Address => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    let mut peer_found = false;

                    for peer in self.peers.clone() {
                        if peer.address.to_string() == address.to_string() {
                            peer_found = true;
                            break;
                        }
                    }

                    if !peer_found {
                        "".to_string()
                    } else {
                        self.address()
                    }
                } else {
                    self.address()
                }
            }
            Command::BurnAddress => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    "".to_string()
                } else {
                    self.burn_address()
                }
            }
            Command::Send => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    if input_args.len() != 2 {
                        "error: invalid arguments: send <amount> <to_address>".to_string()
                    } else {
                        let amount: u128 = input_args[0].parse().unwrap();
                        let to_address = input_args[1];

                        self.send(amount, to_address)
                    }
                }
            }
            Command::Genesis => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    let mut total_supply = std::u128::MAX;

                    if input_args.len() > 1 {
                        "error: invalid arguments: genesis [total_supply | (std::u128::MAX)]"
                            .to_string()
                    } else if input_args.len() == 1 {
                        total_supply = input_args[0].parse().unwrap();
                        self.genesis(total_supply)
                    } else {
                        self.genesis(total_supply)
                    }
                }
            }
            Command::Chain => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }
                }

                self.chain()
            }
            Command::Transaction => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    if input_args.len() != 1 {
                        "error: invalid arguments: transaction <transaction_hash>".to_string()
                    } else {
                        self.transaction(input_args[0])
                    }
                }
            }
            Command::Confirms => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }
                }

                if input_args.len() != 1 {
                    "error: invalid arguments: confirms <transaction_hash>".to_string()
                } else {
                    self.confirms(input_args[0])
                }
            }
            Command::ValidChain => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    if input_args.len() != 0 {
                        "error: this command takes no arguments: validchain".to_string()
                    } else {
                        if self.valid_chain() {
                            "true".to_string()
                        } else {
                            "false".to_string()
                        }
                    }
                }
            }
            Command::Last => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    if input_args.len() > 1 {
                        "error: invalid arguments: last [num_chain_links]".to_string()
                    } else {
                        let mut num_chain_links = 1u128;

                        if input_args.len() > 0 {
                            num_chain_links = input_args[0].parse().unwrap();
                        }

                        self.last(num_chain_links)
                    }
                }
            }
            Command::Balance => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    "".to_string()
                } else {
                    if input_args.len() > 1 {
                        "error: invalid arguments: balance [peer_address]".to_string()
                    } else if input_args.len() == 1 {
                        let coinaddress = input_args[0];

                        let mut peer_found = false;

                        for peer in self.peers.clone() {
                            if peer.address.to_string() == address.to_string() {
                                peer_found = true;
                                break;
                            }
                        }

                        if peer_found || coinaddress == self.burn_address() {
                            format!("{}", self.balance(coinaddress))
                        } else {
                            "".to_string()
                        }
                    } else {
                        format!("{}", self.balance(&self.address()))
                    }
                }
            }
            Command::Get => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }

                    return "".to_string();
                } else {
                    if input_args.len() != 0 {
                        "error: this command takes no arguments: balance".to_string()
                    } else {
                        self.get()
                    }
                }
            }
            Command::Time => {
                format!("{}", self.time())
            }
            Command::Peers => {
                if remote {
                    if !self.check_peer_connection(address, port, true).await {
                        return "".to_string();
                    }
                }

                let mut num_peers = 0;

                if input_args.len() > 1 {
                    return "error: invalid arguments: peers [num_peers]".to_string();
                } else if input_args.len() == 1 {
                    num_peers = input_args[0].parse::<usize>().unwrap_or(0usize);
                }

                self.peers(num_peers, address)
            }
        }
    }

    /// Remove peer from peers list if we can't connect to it, it doesn't reply, or its system time is incorrect.
    pub async fn check_peer_connection(
        &mut self,
        address: IpAddr,
        port: u16,
        remove: bool,
    ) -> bool {
        let mut peer_replied = false;

        let peer_connection_timeout_secs = 3;

        let mut peer_found = false;
        let mut peer_idxs = vec![];
        let mut i = 0;
        let mut ports = vec![];

        for peer in self.peers.clone() {
            if peer.address.to_string() == address.to_string() && peer.port == port {
                peer_found = true;
                peer_idxs.push(i);
                ports.push(peer.port);
                break;
            } else if peer.address.to_string() == address.to_string() {
                peer_found = true;
                peer_idxs.push(i);
                ports.push(peer.port);
            }

            i += 1;
        }

        if peer_found {
            for port in ports {
                match tokio::time::timeout(
                    Duration::from_secs(peer_connection_timeout_secs),
                    TcpStream::connect(&format!("{}:{}", address, port)),
                )
                .await
                {
                    Ok(stream_result) => match stream_result {
                        Ok(mut stream) => {
                            let self_time = self.time() / 10000000;

                            stream
                                .write_all(format!("time").as_bytes())
                                .await
                                .map_or_else(
                                    |err| {
                                        if remove {
                                            for peer_idx in peer_idxs.clone() {
                                                let peer = self.peers.remove(peer_idx);
                                                info!(
                                                    "removing peer: {}:{} : {}",
                                                    peer.address, peer.port, err
                                                );
                                            }
                                        }
                                    },
                                    |_res| {
                                        // async {

                                        // peer_replied = true;
                                        // };

                                        // ()
                                        // await
                                    },
                                );

                            let mut buf = vec![0; 1024];
                            // let mut input_str = "";

                            loop {
                                let n = stream.read(&mut buf).await.unwrap_or_default();

                                if n == 0 {
                                    peer_replied = false;
                                    break;
                                }

                                let mut input_str = str::from_utf8(&buf[0..n]).unwrap_or_default();

                                // Remove whitespace around string if present.
                                input_str = input_str.trim();

                                // Remove newline character if present.
                                if input_str.ends_with("\n") {
                                    input_str = &input_str[0..(input_str.len() - 1)];
                                }

                                // Remove Windows newline character if present.
                                if input_str.ends_with("\r") {
                                    input_str = &input_str[0..(input_str.len() - 1)];
                                }

                                // info!("!! input_str: {}", input_str);

                                let mut peer_time: u128 = input_str.parse().unwrap();
                                peer_time /= 10000000;

                                let time_diff = (self_time).abs_diff(peer_time);

                                if time_diff > 1 {
                                    warn!("peer time is incorrect: {} != {}", peer_time, self_time);

                                    for peer_idx in peer_idxs.clone() {
                                        let peer = self.peers.remove(peer_idx);
                                        info!("removing peer: {}:{}", peer.address, peer.port);
                                    }

                                    peer_replied = false;
                                } else {
                                    peer_replied = true;
                                }

                                break;
                            }

                            // let peer_time: u128 = input_str.parse().unwrap();

                            // if self_time != peer_time {
                            //     warn!("peer time is incorrect: {} != {}", peer_time, self_time);

                            //     for peer_idx in peer_idxs.clone() {
                            //         let peer = self.peers.clone().remove(peer_idx);
                            //         info!("removing peer: {}:{}", peer.address, peer.port);
                            //     }
                            // }
                        }
                        Err(err) => {
                            if remove {
                                for peer_idx in peer_idxs.clone() {
                                    let peer = self.peers.remove(peer_idx);
                                    info!(
                                        "removing peer: {}:{} : failed connecting to peer: {}",
                                        peer.address, peer.port, err
                                    );
                                }
                            }
                        }
                    },
                    Err(e) => {
                        if remove {
                            for peer_idx in peer_idxs.clone() {
                                let peer = self.peers.remove(peer_idx);
                                info!(
                                    "removing peer: {}:{} : timeout connecting to peer: {}",
                                    peer.address, peer.port, e
                                );
                            }
                        }
                    }
                }
            }
        } else {
            match tokio::time::timeout(
                Duration::from_secs(peer_connection_timeout_secs),
                TcpStream::connect(&format!("{}:{}", address, port)),
            )
            .await
            {
                Ok(stream_result) => match stream_result {
                    Ok(mut stream) => stream
                        .write_all(format!("hello {}", port).as_bytes())
                        .await
                        .map_or_else(
                            |_err| {},
                            |_res| {
                                self.peers.push(Peer::new(address, port));

                                peer_replied = true;
                            },
                        ),
                    Err(e) => {
                        error!("failed connecting to peer: {}:{}: {}", address, port, e);
                    }
                },
                Err(e) => {
                    error!("timeout connecting to peer: {}:{}: {}", address, port, e);
                }
            }

            if peer_replied {
                for port in ports {
                    match tokio::time::timeout(
                        Duration::from_secs(peer_connection_timeout_secs),
                        TcpStream::connect(&format!("{}:{}", address, port)),
                    )
                    .await
                    {
                        Ok(stream_result) => match stream_result {
                            Ok(mut stream) => {
                                let self_time = self.time() / 10000000;

                                stream
                                    .write_all(format!("time").as_bytes())
                                    .await
                                    .map_or_else(
                                        |err| {
                                            if remove {
                                                for peer_idx in peer_idxs.clone() {
                                                    let peer = self.peers.remove(peer_idx);
                                                    info!(
                                                        "removing peer: {}:{} : {}",
                                                        peer.address, peer.port, err
                                                    );
                                                }
                                            }
                                        },
                                        |_res| {
                                            // async {

                                            // peer_replied = true;
                                            // };

                                            // ()
                                            // await
                                        },
                                    );

                                let mut buf = vec![0; 1024];
                                // let mut input_str = "";

                                loop {
                                    let n = stream.read(&mut buf).await.unwrap_or_default();

                                    if n == 0 {
                                        peer_replied = false;
                                        break;
                                    }

                                    let mut input_str =
                                        str::from_utf8(&buf[0..n]).unwrap_or_default();

                                    // Remove whitespace around string if present.
                                    input_str = input_str.trim();

                                    // Remove newline character if present.
                                    if input_str.ends_with("\n") {
                                        input_str = &input_str[0..(input_str.len() - 1)];
                                    }

                                    // Remove Windows newline character if present.
                                    if input_str.ends_with("\r") {
                                        input_str = &input_str[0..(input_str.len() - 1)];
                                    }

                                    // info!("!! input_str: {}", input_str);

                                    let mut peer_time: u128 = input_str.parse().unwrap();
                                    peer_time /= 10000000;

                                    let time_diff = (self_time).abs_diff(peer_time);

                                    if time_diff > 1 {
                                        warn!(
                                            "peer time is incorrect: {} != {}",
                                            peer_time, self_time
                                        );

                                        for peer_idx in peer_idxs.clone() {
                                            let peer = self.peers.remove(peer_idx);
                                            info!("removing peer: {}:{}", peer.address, peer.port);
                                        }

                                        peer_replied = false;
                                    } else {
                                        peer_replied = true;
                                    }

                                    break;
                                }

                                // let peer_time: u128 = input_str.parse().unwrap();

                                // if self_time != peer_time {
                                //     warn!("peer time is incorrect: {} != {}", peer_time, self_time);

                                //     for peer_idx in peer_idxs.clone() {
                                //         let peer = self.peers.clone().remove(peer_idx);
                                //         info!("removing peer: {}:{}", peer.address, peer.port);
                                //     }
                                // }
                            }
                            Err(err) => {
                                if remove {
                                    for peer_idx in peer_idxs.clone() {
                                        let peer = self.peers.remove(peer_idx);
                                        info!(
                                            "removing peer: {}:{} : failed connecting to peer: {}",
                                            peer.address, peer.port, err
                                        );
                                    }
                                }
                            }
                        },
                        Err(e) => {
                            if remove {
                                for peer_idx in peer_idxs.clone() {
                                    let peer = self.peers.remove(peer_idx);
                                    info!(
                                        "removing peer: {}:{} : timeout connecting to peer: {}",
                                        peer.address, peer.port, e
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }

        // if peer_replied {
        //     let mut buf = vec![0; 1024];
        //     let mut input_str = "";

        //     loop {
        //         let n = stream.read(&mut buf).await.unwrap_or_default();

        //         if n == 0 {
        //             break;
        //         }

        //         let input_str = str::from_utf8(&buf[0..n]).unwrap_or_default();
        //     }

        //     let peer_time: u128 = input_str.parse().unwrap();

        //     if self_time != peer_time {
        //         warn!("peer time is incorrect: {} != {}", peer_time, self_time);

        //         for peer_idx in peer_idxs.clone() {
        //             let peer = self.peers.clone().remove(peer_idx);
        //             info!("removing peer: {}:{}", peer.address, peer.port);
        //         }
        //     }
        // }

        peer_replied
    }

    pub fn hello(&self) -> String {
        // let peer_replied = self.check_peer_connection(address, port, true).await;

        // if peer_replied {
        "hi!".to_string()
        // } else {
        // "hi?".to_string()
        // }
    }

    /// Returns a hash of the genesis seed.
    pub fn hash_genesis_seed(&self) -> String {
        let mut h = Sha512::new();
        h.update(&self.genesis_seed[..]);

        general_purpose::URL_SAFE_NO_PAD.encode(h.finalize())
    }

    /// Load the keys into memory.
    pub fn load_rsa_keypair(
        &mut self,
        priv_filename: &str,
        pub_filename: &str,
    ) -> pkcs1::Result<()> {
        let mut priv_filename = priv_filename;
        let mut pub_filename = pub_filename;

        if priv_filename == "" {
            priv_filename = "bc.key";
        }

        if pub_filename == "" {
            pub_filename = "bc.key.pub";
        }

        info!("Loading RSA keys: {}, {}", priv_filename, pub_filename);

        let rsa_private_key: RsaPrivateKey =
            pkcs1::DecodeRsaPrivateKey::read_pkcs1_pem_file(priv_filename)?;

        rsa_private_key.validate().unwrap();

        let rsa_public_key: RsaPublicKey =
            pkcs1::DecodeRsaPublicKey::read_pkcs1_pem_file(pub_filename)?;

        self.rsa_private_key = Some(rsa_private_key);
        self.rsa_public_key = Some(rsa_public_key);

        info!("Loaded RSA keys.");

        Ok(())
    }

    /// Load the burn address keys into memory.
    pub fn load_rsa_burn_keypair(
        &mut self,
        priv_filename: &str,
        pub_filename: &str,
    ) -> pkcs1::Result<()> {
        let mut priv_filename = priv_filename;
        let mut pub_filename = pub_filename;

        if priv_filename == "" {
            priv_filename = "bc-burn.key";
        }

        if pub_filename == "" {
            pub_filename = "bc-burn.key.pub";
        }

        info!("Loading RSA keys: {}, {}", priv_filename, pub_filename);

        let rsa_private_key: RsaPrivateKey =
            pkcs1::DecodeRsaPrivateKey::read_pkcs1_pem_file(priv_filename)?;

        rsa_private_key.validate().unwrap();

        let rsa_public_key: RsaPublicKey =
            pkcs1::DecodeRsaPublicKey::read_pkcs1_pem_file(pub_filename)?;

        self.rsa_private_burn_key = Some(rsa_private_key);
        self.rsa_public_burn_key = Some(rsa_public_key);

        info!("Loaded RSA burn keys.");

        Ok(())
    }

    /// Generate new keys.
    pub fn new_rsa_keypair(&mut self) {
        warn!("Generating new RSA keys. This might take a while...");

        let mut rng = rand::thread_rng();

        let bits = 4096;

        let rsa_private_key = RsaPrivateKey::new(&mut rng, bits).expect("failed to generate a key");

        rsa_private_key.validate().unwrap();

        self.rsa_private_key = Some(rsa_private_key);
        self.rsa_public_key = Some(RsaPublicKey::from(self.rsa_private_key.as_ref().unwrap()));

        warn!("Generated new RSA keys.");
    }

    /// Save keys to disk.
    pub fn save_rsa_keypair(&self, priv_filename: &str, pub_filename: &str) -> pkcs1::Result<()> {
        let mut priv_filename = priv_filename;
        let mut pub_filename = pub_filename;

        if priv_filename == "" {
            priv_filename = "bc.key";
        }

        if pub_filename == "" {
            pub_filename = "bc.key.pub"
        }

        info!(
            "Saving RSA keys to files: {}, {}",
            priv_filename, pub_filename
        );

        pkcs1::EncodeRsaPrivateKey::write_pkcs1_pem_file(
            self.rsa_private_key.as_ref().unwrap(),
            priv_filename,
            pkcs1::LineEnding::default(),
        )?;

        pkcs1::EncodeRsaPublicKey::write_pkcs1_pem_file(
            self.rsa_public_key.as_ref().unwrap(),
            pub_filename,
            pkcs1::LineEnding::default(),
        )?;

        info!(
            "Saved RSA keys to files: {}, {}",
            priv_filename, pub_filename
        );

        Ok(())
    }

    /// Returns public key in PEM format.
    pub fn get_rsa_public_key_pem(&self) -> pkcs1::Result<String> {
        pkcs1::EncodeRsaPublicKey::to_pkcs1_pem(
            self.rsa_public_key.as_ref().unwrap(),
            pkcs1::LineEnding::default(),
        )
    }

    /// Returns your receiving address.
    pub fn address(&self) -> String {
        let mut h = Sha256::new();
        h.update(
            format!(
                "{}{}",
                self.genesis_seed_hash,
                self.rsa_public_key
                    .as_ref()
                    .unwrap()
                    .to_pkcs1_pem(pkcs1::LineEnding::default())
                    .unwrap()
            )
            .as_bytes(),
        );

        general_purpose::URL_SAFE_NO_PAD.encode(h.finalize())
    }

    /// Returns the burn address.
    pub fn burn_address(&self) -> String {
        let mut h = Sha256::new();
        h.update(
            format!(
                "{}{}",
                self.genesis_seed_hash,
                self.rsa_public_burn_key
                    .as_ref()
                    .unwrap()
                    .to_pkcs1_pem(pkcs1::LineEnding::default())
                    .unwrap()
            )
            .as_bytes(),
        );

        general_purpose::URL_SAFE_NO_PAD.encode(h.finalize())
    }

    /// Send coins to a receiving address.
    pub fn send(&mut self, amount: u128, to_address: &str) -> String {
        if to_address.to_string() == self.address() {
            return "error: invalid to_address: sending and receiving address are the same"
                .to_string();
        }

        let t = Transaction::new(
            self.genesis_seed_hash.clone(),
            amount,
            &self.address(),
            to_address,
        );

        if !t.valid_to_address() {
            "error: invalid to_address: it should be 43 characters".to_string()
        } else if self.chain.is_none() {
            let _c = self.chain();

            if self.chain.is_none() {
                "error: chain isn't initialized yet. run the \"genesis\" command first.".to_string()
            } else {
                self.send_internal(t)
            }
        } else {
            self.send_internal(t)
        }
    }

    /// Send coins to a receiving address (internal).
    fn send_internal(&mut self, t: Transaction) -> String {
        let db = self.db.db.as_ref().unwrap();

        let chain = ChainLink::new(
            self.genesis_seed_hash.clone(),
            self.chain.as_ref().unwrap().sha512_hash.clone(),
            vec![t.clone()],
        );

        match db.get_cf(db.cf_handle("hash").unwrap(), chain.sha512_hash.as_bytes()) {
            Ok(Some(_c)) => {
                return "error: transaction already exists on chain".to_string();
            }
            Ok(None) => {}
            Err(e) => {
                return format!("error: database operation failed: {}", e);
            }
        }

        db.put_cf(
            db.cf_handle("hash").unwrap(),
            chain.sha512_hash.as_bytes(),
            format!("{}", chain).as_bytes(),
        )
        .unwrap();

        db.put_cf(
            db.cf_handle("timestamp").unwrap(),
            format!("{}", chain.timestamp).as_bytes(),
            chain.sha512_hash.as_bytes(),
        )
        .unwrap();

        db.put_cf(
            db.cf_handle("transaction").unwrap(),
            t.sha512_hash.as_bytes(),
            chain.sha512_hash.as_bytes(),
        )
        .unwrap();

        self.chain = Some(chain);

        format!("{}", t)
    }

    /// Receive coins from the genesis address. Use the `get` command for this.
    pub fn receive(&mut self, amount: u128, to_address: &str) -> String {
        let t = Transaction::new(self.genesis_seed_hash.clone(), amount, "0", to_address);

        if !t.valid_to_address() {
            "error: invalid to_address: it should be 43 characters".to_string()
        } else if self.chain.is_none() {
            let _c = self.chain();

            if self.chain.is_none() {
                "error: chain isn't initialized yet. run the \"genesis\" command first.".to_string()
            } else {
                self.send_internal(t)
            }
        } else {
            self.send_internal(t)
        }
    }

    /// Initialize a new chain.
    pub fn genesis(&mut self, total_supply: u128) -> String {
        let db = self.db.db.as_ref().unwrap();

        match db.get_cf(db.cf_handle("hash").unwrap(), "0") {
            Ok(Some(_c)) => {
                return "error: chain is already initialized".to_string();
            }
            Ok(None) => {}
            Err(e) => {
                return format!("error: database operation failed: {}", e);
            }
        }

        if self.chain.is_some() {
            "error: chain is already initialized".to_string()
        } else {
            let t = Transaction::new(
                self.genesis_seed_hash.clone(),
                cmp::min(total_supply, std::u128::MAX),
                "0",
                "0",
            );

            if !t.valid_to_address() {
                return "error: invalid to_address: it should be 43 characters".to_string();
            }

            let c = ChainLink::new(self.genesis_seed_hash.clone(), "0".to_string(), vec![t]);

            db.put_cf(
                db.cf_handle("hash").unwrap(),
                b"0",
                format!("{}", c).as_bytes(),
            )
            .unwrap();

            db.put_cf(
                db.cf_handle("timestamp").unwrap(),
                format!("{}", c.timestamp).as_bytes(),
                b"0",
            )
            .unwrap();

            let res;

            match db.get_cf(db.cf_handle("hash").unwrap(), b"0") {
                Ok(Some(c)) => res = str::from_utf8(&c[..]).unwrap().to_string(),
                Ok(None) => res = "error: key not found in database: \"0\"".to_string(),
                Err(e) => res = format!("error: database operation failed: {}", e),
            }

            self.chain = Some(serde_json::from_str(res.as_str()).unwrap());

            format!("{}", res)
        }
    }

    /// Returns the most recent chain link.
    pub fn chain(&mut self) -> String {
        let db = self.db.db.as_ref().unwrap();

        let iter = db.iterator_cf(
            db.cf_handle("timestamp").unwrap(),
            IteratorMode::From(
                format!(
                    "{}",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos()
                )
                .as_bytes(),
                Direction::Reverse,
            ),
        );

        let res;

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => res = str::from_utf8(&c[..]).unwrap().to_string(),
                Ok(None) => res = "{}".to_string(),
                Err(e) => res = format!("error: database operation failed: {}", e),
            }

            self.chain = Some(serde_json::from_str(res.as_str()).unwrap());

            return format!("{}", res);
        }

        format!("{{}}")
    }

    /// Returns the chain link containing the given `transaction_hash`.
    pub fn transaction(&mut self, transaction_hash: &str) -> String {
        let db = self.db.db.as_ref().unwrap();

        let chain_hash;

        match db.get_cf(
            db.cf_handle("transaction").unwrap(),
            transaction_hash.as_bytes(),
        ) {
            Ok(Some(c)) => chain_hash = str::from_utf8(&c[..]).unwrap().to_string(),
            Ok(None) => chain_hash = "{}".to_string(),
            Err(e) => chain_hash = format!("error: database operation failed: {}", e),
        }

        let res;

        match db.get_cf(db.cf_handle("hash").unwrap(), chain_hash.as_bytes()) {
            Ok(Some(c)) => res = str::from_utf8(&c[..]).unwrap().to_string(),
            Ok(None) => res = "{}".to_string(),
            Err(e) => res = format!("error: database operation failed: {}", e),
        }

        res
    }

    /// Returns the number of confirms available for a given `transaction_hash`.
    /// This is the number of chain links which exist after the one containing
    /// the transaction.
    pub fn confirms(&mut self, transaction_hash: &str) -> String {
        let db = self.db.db.as_ref().unwrap();

        let iter = db.iterator_cf(
            db.cf_handle("timestamp").unwrap(),
            IteratorMode::From(
                format!(
                    "{}",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos()
                )
                .as_bytes(),
                Direction::Reverse,
            ),
        );

        let mut confirms = 0u128;

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => {
                    let chain: ChainLink =
                        serde_json::from_str(str::from_utf8(&c[..]).unwrap()).unwrap();

                    for t in chain.transactions {
                        if t.sha512_hash == transaction_hash {
                            return format!("{}", confirms);
                        }
                    }

                    confirms += 1;
                }
                Ok(None) => {}
                Err(e) => {
                    return format!("error: database operation failed: {}", e);
                }
            }
        }

        format!("{}", confirms)
    }

    /// Check the hashes of every chain link to make sure they are correct.
    /// This rehashes each chain link and compares the new computed hash
    /// with the hash saved in the chain link.
    pub fn valid_chain(&self) -> bool {
        let db = self.db.db.as_ref().unwrap();

        let iter = db.iterator_cf(
            db.cf_handle("timestamp").unwrap(),
            IteratorMode::From(
                format!(
                    "{}",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos()
                )
                .as_bytes(),
                Direction::Reverse,
            ),
        );

        let mut res = false;

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => {
                    // validate hash
                    let mut chain: ChainLink =
                        serde_json::from_str(str::from_utf8(&c[..]).unwrap()).unwrap();

                    let valid_hash = chain.sha512_hash;

                    chain.sha512_hash = "".to_string();

                    let chain_string = serde_json::to_string(&chain).unwrap();

                    let mut h = Sha512::new();
                    h.update(format!("{}{}", self.genesis_seed_hash, chain_string).as_bytes());
                    let computed_hash = general_purpose::URL_SAFE_NO_PAD.encode(h.finalize());

                    if valid_hash == computed_hash {
                        res = true;
                    } else {
                        res = false;

                        warn!("computed_hash invalid: chain: {}", chain);
                        warn!("computed_hash invalid: computed_hash: {}", computed_hash);
                        warn!("computed_hash invalid: valid_hash: {}", valid_hash);

                        break;
                    }
                }
                Ok(None) => {}
                Err(e) => {
                    error!("error: database operation failed: {}", e);
                }
            }
        }

        res
    }

    /// Returns the most recent `num_chain_links` chain links in the chain.
    /// If you use `num_chain_links = 0`, the entire chain will be returned.
    pub fn last(&self, num_chain_links: u128) -> String {
        let db = self.db.db.as_ref().unwrap();

        let iter = db.iterator_cf(
            db.cf_handle("timestamp").unwrap(),
            IteratorMode::From(
                format!(
                    "{}",
                    SystemTime::now()
                        .duration_since(UNIX_EPOCH)
                        .unwrap()
                        .as_nanos()
                )
                .as_bytes(),
                Direction::Reverse,
            ),
        );

        let mut res: Vec<ChainLink> = vec![];

        let mut i = 0;
        let mut dont_stop = false;

        if num_chain_links == 0 {
            dont_stop = true;
        }

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => {
                    if !dont_stop && i >= num_chain_links {
                        break;
                    }

                    let chain: ChainLink =
                        serde_json::from_str(str::from_utf8(&c[..]).unwrap()).unwrap();

                    res.push(chain);

                    i += 1;
                }
                Ok(None) => {}
                Err(e) => {
                    error!("error: database operation failed: {}", e);
                }
            }
        }

        serde_json::to_string_pretty(&res).unwrap()
    }

    pub fn balance(&self, address: &str) -> u128 {
        // TODO: Save balance check in db with timestamp so we don't have to scan the whole chain every time.

        let db = self.db.db.as_ref().unwrap();

        let iter = db.iterator_cf(db.cf_handle("timestamp").unwrap(), IteratorMode::Start);

        let mut res = 0u128;

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => {
                    let chain: ChainLink =
                        serde_json::from_str(str::from_utf8(&c[..]).unwrap()).unwrap();

                    for transaction in chain.transactions {
                        if transaction.from_address == transaction.to_address {
                            continue;
                        }

                        if transaction.from_address == address.to_string() {
                            // if transaction.from_address == self.address() {
                            if res >= transaction.amount {
                                res -= transaction.amount;
                            }
                        }

                        if transaction.to_address == address.to_string() {
                            // if transaction.to_address == self.address() {
                            if res + transaction.amount <= std::u128::MAX {
                                res += transaction.amount;
                            }
                        }
                    }
                }
                Ok(None) => {}
                Err(e) => {
                    error!("error: database operation failed: {}", e);
                }
            }
        }

        res
    }

    pub fn get(&mut self) -> String {
        // TODO: Prove that our system time is correct before being eligible to get.

        let daily_amount = 1000u128;

        let db = self.db.db.as_ref().unwrap();

        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos();

        let day = 86400000000000u128;
        let yesterday = now - day;

        let iter = db.iterator_cf(
            db.cf_handle("timestamp").unwrap(),
            IteratorMode::From(format!("{}", now).as_bytes(), Direction::Reverse),
        );

        let mut eligible = true;
        let mut last_get = 0u128;

        for item in iter {
            let (_key, val) = item.unwrap();

            match db.get_cf(db.cf_handle("hash").unwrap(), &val[..]) {
                Ok(Some(c)) => {
                    let chain: ChainLink =
                        serde_json::from_str(str::from_utf8(&c[..]).unwrap()).unwrap();

                    if chain.timestamp <= yesterday {
                        break;
                    }

                    for transaction in chain.transactions {
                        if transaction.from_address == "0".to_string()
                            && transaction.to_address == self.address()
                        {
                            eligible = false;
                            last_get = transaction.timestamp;
                            break;
                        }
                    }
                }
                Ok(None) => {}
                Err(e) => {
                    error!("error: database operation failed: {}", e);
                }
            }
        }

        if eligible {
            self.receive(daily_amount, &self.address())
        } else {
            format!(
                "error: time remaining until you can use this command to get coins again (ns): {}",
                (last_get + day) - now
            )
        }
    }

    pub fn time(&self) -> u128 {
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_nanos()
    }

    pub fn peers(&self, num_peers: usize, address: IpAddr) -> String {
        let mut peer_found = false;

        for peer in self.peers.clone() {
            if peer.address.to_string() == address.to_string() {
                peer_found = true;
                break;
            }
        }

        if !address.is_loopback() && !peer_found {
            return "[]".to_string();
        }

        let mut peers = self.peers.clone();

        if num_peers > 0 && peers.len() >= num_peers {
            peers = peers[..num_peers].to_vec();
        }

        serde_json::to_string_pretty(&peers).unwrap()
    }
}
