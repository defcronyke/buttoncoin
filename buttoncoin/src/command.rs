use strum_macros::EnumString;

#[derive(EnumString)]
pub enum Command {
    #[strum(disabled)]
    Invalid,
    #[strum(ascii_case_insensitive)]
    Hello,
    #[strum(ascii_case_insensitive)]
    RsaPubKey,
    #[strum(ascii_case_insensitive)]
    Address,
    #[strum(ascii_case_insensitive)]
    BurnAddress,
    #[strum(ascii_case_insensitive)]
    Send,
    #[strum(ascii_case_insensitive)]
    Genesis,
    #[strum(ascii_case_insensitive)]
    Chain,
    #[strum(ascii_case_insensitive)]
    Transaction,
    #[strum(ascii_case_insensitive)]
    Confirms,
    #[strum(ascii_case_insensitive)]
    ValidChain,
    #[strum(ascii_case_insensitive)]
    Last,
    #[strum(ascii_case_insensitive)]
    Balance,
    #[strum(ascii_case_insensitive)]
    Get,
    #[strum(ascii_case_insensitive)]
    Time,
    #[strum(ascii_case_insensitive)]
    Peers,
}
