use serde::{Deserialize, Serialize};
use std::fmt;
use std::net::IpAddr;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Peer {
    pub address: IpAddr,
    pub port: u16,
}

impl fmt::Display for Peer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}

impl Peer {
    pub fn new(address: IpAddr, port: u16) -> Peer {
        Peer { address, port }
    }
}
