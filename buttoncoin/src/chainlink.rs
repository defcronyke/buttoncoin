use crate::transaction::Transaction;
use base64::{engine::general_purpose, Engine as _};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512};
use std::fmt;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Serialize, Deserialize, Debug)]
pub struct ChainLink {
    pub prev_sha512_hash: String,
    pub timestamp: u128,
    pub transactions: Vec<Transaction>,
    pub sha512_hash: String,
}

impl fmt::Display for ChainLink {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string(self).unwrap())
    }
}

impl ChainLink {
    pub fn new(
        genesis_seed_hash: String,
        prev_sha512_hash: String,
        transactions: Vec<Transaction>,
    ) -> ChainLink {
        let mut c = ChainLink {
            prev_sha512_hash,
            timestamp: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_nanos(),
            transactions,
            sha512_hash: "".to_string(),
        };

        let mut h = Sha512::new();
        h.update(format!("{}{}", genesis_seed_hash, c).as_bytes());
        c.sha512_hash = general_purpose::URL_SAFE_NO_PAD.encode(h.finalize());

        c
    }
}
