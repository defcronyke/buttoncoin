#!/bin/bash

if [ $# -lt 1 ]; then
  echo "usage: $0 <command> [-s remote_ip] [args...]"
fi

if [ $# -gt 2 -a $1 == "-s" ]; then
  remote="$2"
  shift
  shift
  echo -n "$@" | nc -q 0 "$remote" 8081
else
  echo -n "$@" | nc -q 0 localhost 8081
fi
